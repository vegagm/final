import sys
from transforms import change_colors, rotate_colors, shift, crop

from images import read_img, write_img, create_blank


def main():
    print(sys.argv[1])
    print(sys.argv[2])
    image = read_img(sys.argv[1])

    if sys.argv[2] == "change_colors":
        image_trans = change_colors(image,(int(sys.argv[3]),int(sys.argv[4]), int(sys.argv[5])),( int(sys.argv[6]), int(sys.argv[7]), int(sys.argv[8])))
    elif sys.argv[2] == "rotate_colors":
        image_trans = rotate_colors(image, int(sys.argv[3]))
    elif sys.argv[2] == "shift":
        image_trans = shift(image, int(sys.argv[3]), int(sys.argv[4]))
    elif sys.argv[2] == "crop":
        image_trans = crop(image, int(sys.argv[3]), int(sys.argv[4]),int(sys.argv[5]),int(sys.argv[6]),)
    else:
        print('transformación no válida')
    '''
    pruebas Vega
    for fila in image:
        for pixel in fila:
            print (pixel, end=' ' )
        print()
    for fila in image_trans:
        for pixel in fila:
            print (pixel, end=' ' )
        print()
'''
    file_name=sys.argv[1].replace(".","_trans.")
    write_img(image_trans, file_name)


if __name__ == '__main__':
    main()