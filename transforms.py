from images import *
from images import read_img, write_img, create_blank, size

#método change_colors
def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
#Produce una imagen con los colores cambiados si el color es to change, se cambia por to_change_to

    (width, height) = size(image)
    for x in range(width):
        for y in range(height):
            if image[x][y] == to_change:
                image[x][y] = to_change_to

    return image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
#Crear una nueva imagen girada 90 grados hacia la derecha
    (width, height) = size(image)
    image_rotated = create_blank(height, width)
    for x in range(width):
        for y in range(height):
            image_rotated[height-1-y][x] = image[x][y]
    return image_rotated
def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
#Crear una imagen espejada segun un eje vertical situado en la mitad de la imagen
    (width, height) = size(image)
    image_mirrored = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            image_mirrored[x][y] = image[width - 1 - x][y]
    return image_mirrored

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
#Crear una nueva imagen con los colores cambiados sumando el numero que indique como incremento

    (width, height) = size(image)
    image_rotated_colors = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            R = (image[x][y][0] + increment) % 255
            G = (image[x][y][1] + increment) % 255
            B = (image[x][y][2] + increment) % 255
            image_rotated_colors[x][y] = (R, G, B)

    return image_rotated_colors

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''
    Crea una nueva imagen donde el valor RGB de cada pixelserá el valor medio de los
    valores RGB de los pixels que tiene encima, a la derecha y a la izquierda.
    Hay que tener en cuenta las filas de arriba, abajo y los pixeles iniciales y finales de las filas.
    '''
    (width, height) = size(image)
    image_blur = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            if y == 0:
                if x == 0:
                    R = int((image[x + 1][y][0] + image[x][y + 1][0]) / 2)
                    G = int((image[x + 1][y][1] + image[x][y + 1][1]) / 2)
                    B = int((image[x + 1][y][2] + image[x][y + 1][2]) / 2)
                elif x == width - 1:
                    R = int((image[x - 1][y][0] + image[x][y + 1][0]) / 2)
                    G = int((image[x - 1][y][1] + image[x][y + 1][1]) / 2)
                    B = int((image[x - 1][y][2] + image[x][y + 1][2]) / 2)
                else:
                    R = int((image[x - 1][y][0] + image[x + 1][y][0] + image[x][y + 1][0]) / 3)
                    G = int((image[x - 1][y][1] + image[x + 1][y][1] + image[x][y + 1][1]) / 3)
                    B = int((image[x - 1][y][2] + image[x + 1][y][2] + image[x][y + 1][2]) / 3)

            elif y == height - 1:
                if x == 0:
                    R = int((image[x + 1][y][0] + image[x][y - 1][0]) / 2)
                    G = int((image[x + 1][y][1] + image[x][y - 1][1]) / 2)
                    B = int((image[x + 1][y][2] + image[x][y - 1][2]) / 2)
                elif x == width - 1:
                    R = int((image[x - 1][y][0] + image[x][y - 1][0]) / 2)
                    G = int((image[x - 1][y][1] + image[x][y - 1][1]) / 2)
                    B = int((image[x - 1][y][2] + image[x][y - 1][2]) / 2)
                else:
                    R = int((image[x - 1][y][0] + image[x + 1][y][0] + image[x][y - 1][0]) / 3)
                    G = int((image[x - 1][y][1] + image[x + 1][y][1] + image[x][y - 1][1]) / 3)
                    B = int((image[x - 1][y][2] + image[x + 1][y][2] + image[x][y - 1][2]) / 3)
            else:
                if x == 0:
                    R = int((image[x + 1][y][0] + image[x][y - 1][0] + image[x][y + 1][0]) / 3)
                    G = int((image[x + 1][y][1] + image[x][y - 1][1] + image[x][y + 1][1]) / 3)
                    B = int((image[x + 1][y][2] + image[x][y - 1][2] + image[x][y + 1][2]) / 3)
                elif x == width - 1:
                    R = int((image[x - 1][y][0] + image[x][y - 1][0] + image[x][y + 1][0]) / 3)
                    G = int((image[x - 1][y][1] + image[x][y - 1][1] + image[x][y + 1][1]) / 3)
                    B = int((image[x - 1][y][2] + image[x][y - 1][2] + image[x][y + 1][2]) / 3)

                else:
                    R = int((image[x - 1][y][0] + image[x + 1][y][0] + image[x][y - 1][0] + image[x][y + 1][0]) / 4)
                    G = int((image[x - 1][y][1] + image[x + 1][y][1] + image[x][y - 1][1] + image[x][y + 1][1]) / 4)
                    B = int((image[x - 1][y][2] + image[x + 1][y][2] + image[x][y - 1][2] + image[x][y + 1][1]) / 4)
            image_blur[x][y] = (R, G, B)
    return image_blur


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    '''
    Desplaza la imagen el número de pixeles que se indica en el eje horizontal o vertical
    :param image: imagen a modificar
    :param horizontal: pixels a desplazar horizontalmente
    :param vertical: pixels a desplazar verticalmente
    :return: imagen desplazada
    '''

    (width, height) = size(image)
    image_shift = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            image_shift[x][y]=image[(x+horizontal)%width][(y+vertical)%height]
    return image_shift

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    '''
    Devuelve una imagen que contiene sólo los pixels que se encuentr dentro
    de un rectángulo que se especifique
    :param image:
    :param x:
    :param y:
    :param width:
    :param height:
    :return:
    '''

    image_crop = create_blank(width, height)
    for i in range(width):
        for j in range(height):
            image_crop[i][j] = image[x+i][y+j]
    return image_crop

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''
    Crea una nueva imagen que contiene la imagen original pero en escala de grises
    :param image:
    :return:
    '''

    (width, height) = size(image)
    image_grayscale = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            R = image[x][y][0]
            G = image[x][y][1]
            B = image[x][y][2]
            C = int((R + G + B) / 3)
            image_grayscale[x][y] = (C, C, C)
    return image_grayscale
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    '''
    Crea una imagen con un filtro aplicado a la imagen que se especifica como un
    multiplicador
    :param image:
    :param r:
    :param b:
    :return:
    '''

    (width, height) = size(image)
    image_filtered = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            R = min(int(image[x][y][0] * r), 255)
            G = min(int(image[x][y][1] * g), 255)
            B = min(int(image[x][y][2] * b), 255)

            image_filtered[x][y] = (R, G, B)
    return image_filtered