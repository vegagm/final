import sys
from transforms import *

from images import read_img, write_img, create_blank


def main():
    image = read_img(sys.argv[1])
    multi_trans = sys.argv[2:]

    i = 0
    salto = 0
    while (i < len(multi_trans)):

        if multi_trans[i] == 'rotate_right':
            image_trans = rotate_right(image)
            salto = 1

        elif multi_trans[i] == 'mirror':
            image_trans = mirror(image)
            salto = 1

        elif multi_trans[i] == 'blur':
            image_trans = blur(image)
            salto = 1

        elif multi_trans[i] == 'greyscale':
            image_trans = grayscale(image)
            salto = 1

        elif multi_trans[i] == 'change_colors':
            image_trans = change_colors(image,(int(multi_trans[i + 1]), int(multi_trans[i + 2]), int(multi_trans[i + 3])),(int(multi_trans[i + 4]), int(multi_trans[i + 5]), int(multi_trans[i + 6])))
            salto = 7

        elif multi_trans[i] == 'rotate_colors':
            image_trans = rotate_colors(image, int(multi_trans[i + 1]))
            salto = 2

        elif multi_trans[i] == 'shift':
            image_trans = shift(image, int(multi_trans[i + 1]), int(multi_trans[i + 2]))
            salto = 3

        elif multi_trans[i] == 'crop':
            image_trans = crop(image, int(multi_trans[i + 1]), int(multi_trans[i + 2]), int(multi_trans[i + 3]),
                               int(multi_trans[i + 4]))
            salto = 5
        else:
            print("la función no existe")

        i = i + salto
        image = image_trans



    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)


if __name__ == '__main__':
    main()